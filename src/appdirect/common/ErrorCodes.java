package appdirect.common;

/**
 * All the required Error codes are defined in this Enum.
 * 
 * @author apurv
 */
public enum ErrorCodes {

    ACCOUNT_NOT_FOUND("Account not found for given accountId."),
    USER_ALREADY_EXISTS("The user with given accountId already exists."),
    SOURCE_NOT_VALID("Error while subscribing event as the request is from invalid source"),
    UNKNOWN("UNKNOWN");

    private String message;

    private ErrorCodes(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}