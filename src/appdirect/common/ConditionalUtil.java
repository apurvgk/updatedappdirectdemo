package appdirect.common;

import java.util.List;

/**
 * Defines utility methods which are frequently used in application
 * 
 * @author apurv
 */
public class ConditionalUtil {

    public static final boolean isNullOrEmpty(String str) {
        if (null != str && !str.isEmpty()) {
            return false;
        }
        return true;
    }

    public static final <T extends Object> boolean isNullOrEmpty(List<T> list) {
        if (null != list && !list.isEmpty()) {
            return false;
        }
        return true;
    }
}