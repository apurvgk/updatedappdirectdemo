package appdirect.common;

/**
 * Defines all the constants which are frequently required in application
 * 
 * @author apurv
 */
public class AppConstants {

    public static final String SUBSCRIPTION_EVENT_CACHE = "subscriptionEventCache";

    public static final String USER_CACHE = "usersCache";

    public static final String SUBS_ORDER_SUCCESS = "Subscription order added";

    public static final String SUBS_CHANGE_SUCCESS = "Subscription order Changed";

    public static final String SUBS_CANCELED_SUCCESS = "Subscription order cancelled";

    public static final String ACC_STATUS_ACTIVE = "ACTIVE";

    public static final String PREFIX_FOR_ACC_ID = "SE";

    public static final String EXCEPTION_PREFIX = "EXCEPTION: ";

    public static final String APPLICATION_HOME_PAGE_JSP = "applicationHomePage";

    public static final String ACCESS_DENIED_JSP = "accessDenied";

    public static final String OPEN_ID_CODE_REGEX = "/id/";

    public static final String LOGIN_JSP = "login";

    public static final String INVALID = "INVALID";

    public static final CharSequence ACC_ID_REPLACE_STRING = "<##accountId##>";

}
