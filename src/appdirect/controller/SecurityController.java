package appdirect.controller;

import org.apache.log4j.Logger;
import org.openid4java.consumer.ConsumerException;
import org.openid4java.discovery.DiscoveryException;
import org.openid4java.message.MessageException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import appdirect.common.AppConstants;
import appdirect.service.SecurityService;

/**
 * This controller handles the security related requests.
 * 
 * @author apurv
 */
@Controller
public class SecurityController {

    private static final String OPEN_ID_LOGIN_URL = "openIDLoginUrl";

    @Autowired
    private SecurityService service;

    protected static Logger LOGGER = Logger.getLogger("securityController");

    /**
     * Login method for application, also Called when a user click on application thumbnail from marketplace.
     * 
     * @param openId
     * @param accountId
     * @param model
     * @return Application Login page for authentication.
     */
    @RequestMapping(method = RequestMethod.GET, value = "/login")
    public String login(@RequestParam(value = "openid_url") String openId, @RequestParam String accountId, final Model model) {
        LOGGER.debug("login called for openidUrl: " + openId + " and accountId:" + accountId);
        try {
            String url = getService().fetchOpenIdLoginURL(openId,accountId);
            model.addAttribute(OPEN_ID_LOGIN_URL, url);
            model.addAttribute("userId", accountId);
            LOGGER.debug("Authenticating with openIdLoginURL: " + url);
        }
        catch (DiscoveryException | MessageException | ConsumerException e) {
            LOGGER.debug(AppConstants.EXCEPTION_PREFIX + e.getMessage());
            LOGGER.error(e.getStackTrace());
            return AppConstants.ACCESS_DENIED_JSP;
        }
        return AppConstants.LOGIN_JSP;
    }

    public SecurityService getService() {
        return service;
    }

    public void setService(SecurityService service) {
        this.service = service;
    }
}