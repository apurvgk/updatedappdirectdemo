package appdirect.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import appdirect.common.AppConstants;
import appdirect.service.SecurityService;

/**
 * Implements methods related to application operations.
 * 
 * @author apurv
 */
@Controller
@RequestMapping(value = "/myApplication")
public class ApplicationController {

    private static Logger LOGGER = Logger.getLogger("applicationController");

    private static final String OPENID_CLAIMED_ID = "openid.claimed_id";

    private static final String OPENID_SREG_EMAIL = "openid.sreg.email";

    @Autowired
    private SecurityService service;

    @RequestMapping(method = RequestMethod.GET, value = "/home")
    public String showHomePage(@RequestParam(value = OPENID_SREG_EMAIL) String email, @RequestParam(value = OPENID_CLAIMED_ID) String openId) {
        if (null == email || null == openId) {
            LOGGER.debug("Access Denied for user with email: " + email + " and openId: " + openId);
            return AppConstants.ACCESS_DENIED_JSP;
        }
        getService().saveUserInfo(email, openId);
        return AppConstants.APPLICATION_HOME_PAGE_JSP;
    }

    public SecurityService getService() {
        return service;
    }

    /*
     * Kept a setter so that we can mock service in Junit tests.
     */
    public void setService(SecurityService service) {
        this.service = service;
    }
}