package appdirect.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import appdirect.common.AppConstants;
import appdirect.common.ErrorCodes;
import appdirect.model.EventResponse;
import appdirect.model.SubscriptionEvent;
import appdirect.service.SecurityService;
import appdirect.service.SubscriptionService;

@Controller
@RequestMapping("/subscription")
public class SubscriptionController {

    private static Logger LOGGER = Logger.getLogger("subscriptionController");

    @Autowired
    private SubscriptionService service;

    @Autowired
    private SecurityService securityService;

    @RequestMapping(method = RequestMethod.GET, value = "/create", produces = "application/json")
    public @ResponseBody EventResponse createSubscription(@RequestParam String eventUrl, @RequestParam String token) {
        LOGGER.debug("createSubscription called for eventUrl: " + eventUrl + " and token:" + token);
        if (getSecurityService().isMarketplaceValid(eventUrl)) {
            SubscriptionEvent subscriptionEvent = getService().fetchSubscriptionEventDetails(eventUrl);
            EventResponse evResponse = getService().saveSubscriptionEventDetais(subscriptionEvent);
            if (null == evResponse.getErrorCode() && !getSecurityService().isAnExistingUserInDatastore(evResponse.getAccountIdentifier())) {
                LOGGER.debug("Creating a new user with accountIdentifier: " + evResponse.getAccountIdentifier());
                getSecurityService().createUserInUserCache(evResponse.getAccountIdentifier(), subscriptionEvent);
            }
            return evResponse;
        }
        LOGGER.debug("Error while subscribing new user, as the request is from invalid source: " + eventUrl);
        return getService().createAndReturnErrorResponse(AppConstants.INVALID, ErrorCodes.SOURCE_NOT_VALID);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/change", produces = "application/json")
    public @ResponseBody EventResponse changeSubscription(@RequestParam String eventUrl, @RequestParam String token) {
        LOGGER.debug("changeSubscription called for eventUrl: " + eventUrl + " and token:" + token);
        if (getSecurityService().isMarketplaceValid(eventUrl)) {
            SubscriptionEvent subscriptionEvent = getService().fetchSubscriptionEventDetails(eventUrl);
            EventResponse evResponse = getService().updateSubscriptionEventDetais(subscriptionEvent);
            return evResponse;
        }
        LOGGER.debug("Change subscription failed, as the request is from invalid source: " + eventUrl);
        return getService().createAndReturnErrorResponse(AppConstants.INVALID, ErrorCodes.SOURCE_NOT_VALID);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/cancel", produces = "application/json")
    public @ResponseBody EventResponse cancelSubscription(@RequestParam String eventUrl, @RequestParam String token) {
        LOGGER.debug("cancelSubscription called for eventUrl: " + eventUrl + " and token:" + token);
        if (getSecurityService().isMarketplaceValid(eventUrl)) {
            EventResponse evResponse = getService().cancelSubscriptionEventDetais(eventUrl);
            if (null == evResponse.getErrorCode() && getSecurityService().isAnExistingUserInDatastore(evResponse.getAccountIdentifier())) {
                LOGGER.debug("Removing user with accountIdentifier: " + evResponse.getAccountIdentifier() + " from user cache.");
                getSecurityService().removeUserFromCache(evResponse.getAccountIdentifier());
            }
            return evResponse;
        }
        LOGGER.debug("Cancel subscription failed for eventURL: " + eventUrl);
        return getService().createAndReturnErrorResponse(AppConstants.INVALID, ErrorCodes.SOURCE_NOT_VALID);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/eventInfo", produces = "application/json")
    public @ResponseBody SubscriptionEvent fetchEventInfo(@RequestParam String accountIdentifier) {
        SubscriptionEvent eventDetails = getService().fetchEventInfoFromAccId(accountIdentifier);
        return eventDetails;
    }

    public SubscriptionService getService() {
        return service;
    }

    public void setService(SubscriptionService service) {
        this.service = service;
    }

    public SecurityService getSecurityService() {
        return securityService;
    }

    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }
}