package appdirect.model;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class Order implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String editionCode;

    private String addonOfferingCode;

    private String pricingDuration;

    public String getEditionCode() {
        return editionCode;
    }

    public void setEditionCode(String editionCode) {
        this.editionCode = editionCode;
    }

    public String getAddonOfferingCode() {
        return addonOfferingCode;
    }

    public void setAddonOfferingCode(String addonOfferingCode) {
        this.addonOfferingCode = addonOfferingCode;
    }

    public String getPricingDuration() {
        return pricingDuration;
    }

    public void setPricingDuration(String pricingDuration) {
        this.pricingDuration = pricingDuration;
    }

}
