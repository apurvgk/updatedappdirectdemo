package appdirect.model;

public enum MarketPlaces {

    APPDIRECT("https://marketplace.appdirect.com"),
    TEST("http://localhost:8080");

    private String domainName;

    public String getDomainName() {
        return domainName;
    }

    private MarketPlaces(String domainName) {
        this.domainName = domainName;
    }

    public String getDomainName(MarketPlaces marketPlace) {
        return marketPlace.getDomainName();
    }
}
