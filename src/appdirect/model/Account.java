package appdirect.model;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class Account implements Serializable{
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String accountIdentifier;
    
    private String status;
    
    public String getAccountIdentifier() {
        return accountIdentifier;
    }

    public void setAccountIdentifier(String accountIdentifier) {
        this.accountIdentifier = accountIdentifier;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}