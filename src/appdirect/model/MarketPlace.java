package appdirect.model;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class MarketPlace implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String partner;

    private String baseUrl;

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

}
