package appdirect.model;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class Notice implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private NoticeType type;

    private String message;

    public NoticeType getType() {
        return type;
    }

    public void setType(NoticeType type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
