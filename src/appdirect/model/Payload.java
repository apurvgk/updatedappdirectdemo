package appdirect.model;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class Payload implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private User[] users;

    private Company company;

    private Order order;

    private Notice notice;
    
    public User[] getUsers() {
        return users;
    }

    public void setUsers(User[] users) {
        this.users = users;
    }
    
    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Notice getNotice() {
        return notice;
    }

    public void setNotice(Notice notice) {
        this.notice = notice;
    }

}
