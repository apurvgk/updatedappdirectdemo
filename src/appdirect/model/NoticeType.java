package appdirect.model;

public enum NoticeType {

    REACTIVATED,
    DEACTIVATED,
    CLOSED

}
