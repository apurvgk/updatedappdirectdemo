package appdirect.dao;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.stereotype.Repository;

import appdirect.common.AppConstants;
import appdirect.model.SubscriptionEvent;
import appdirect.model.User;

@Repository
public class SecurityDAOImpl implements SecurityDao {

    private static Logger LOGGER = Logger.getLogger("securityDAOImpl");

    @Resource(name = "cacheManager")
    private EhCacheCacheManager cacheManager;

    @Override
    public void saveUserInfo(User user, String accountIdentifier) {
        Cache cache = getCacheManager().getCache(AppConstants.USER_CACHE);
        cache.put(accountIdentifier, user);
    }

    @Override
    public boolean isAnExistingUserInDatastore(String accountIdentifier) {
        Cache cache = getCacheManager().getCache(AppConstants.USER_CACHE);
        ValueWrapper valueWrapper = cache.get(accountIdentifier);
        LOGGER.debug("valueWrapper for accountIdentifier: " + accountIdentifier + " is: " + valueWrapper);
        return (null != valueWrapper);
    }

    public EhCacheCacheManager getCacheManager() {
        return cacheManager;
    }

    public void setCacheManager(EhCacheCacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    @Override
    public void createUserInUserCache(String accountIdentifier, User user) {
        Cache cache = getCacheManager().getCache(AppConstants.USER_CACHE);
        cache.put(accountIdentifier, user);
        LOGGER.debug("User with accountIdentifier: " + accountIdentifier + " saved in user cache");
    }

    @Override
    public void removeUserFromDatasource(String accountIdentifier) {
        Cache cache = getCacheManager().getCache(AppConstants.USER_CACHE);
        cache.evict(accountIdentifier);
        LOGGER.debug("Record for user with account id: " + accountIdentifier + " deleted successfully.");
    }
}