package appdirect.dao;

import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.stereotype.Repository;

import appdirect.model.EventResponse;
import appdirect.model.SubscriptionEvent;

@Repository
public interface SubcriptionDao {

    /**
     * Save event details in subscriptionEventCache with accountIdentifier as key.
     * @param eventDetails
     * @return EventResponse
     */
    EventResponse saveSubscriptionEventDetais(SubscriptionEvent eventDetails);

    /**
     * Fetch SubscriptionEvent for provided accountIdentifier
     * @param accountIdentifier
     * @return SubscriptionEvent
     */
    SubscriptionEvent fetchEventInfoFromAccId(String accountIdentifier);

    /**
     * Update SubscriptionEvent details for given accountId in subscriptionEventCache
     * @param eventDetails
     * @param accIdentifier
     * @return EventResponse
     */
    EventResponse updateSubscriptionEventDetais(SubscriptionEvent eventDetails, String accIdentifier);

    /**
     * Evicts the SubscriptionEvent details for given accountIdentifier
     * @param accIdentifier
     * @return EventResponse
     */
    EventResponse cancelSubscriptionEventDetais(String accIdentifier);

    /**
     * Fetch ValueWrapper, required to check if Subscription event already present for given accountIdentifier
     * @param cacheName
     * @param accountIdentifier
     * @return ValueWrapper
     */
    ValueWrapper fetchValueWrapperForEvent(String cacheName, String accountIdentifier);
}
