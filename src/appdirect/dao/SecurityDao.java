package appdirect.dao;

import org.springframework.stereotype.Repository;

import appdirect.model.User;

/**
 * DAO operations for application security.
 * 
 * @author apurv
 */
@Repository
public interface SecurityDao {

    /**
     * Save User object in user cache.
     * 
     * @param email
     * @param accountIdentifier
     */
    void saveUserInfo(User user, String accountIdentifier);

    /**
     * Search through Usercache and returns true if user for given accountIdentifier is NOT present
     * 
     * @param accountIdentifier
     */
    boolean isAnExistingUserInDatastore(String accountIdentifier);

    /**
     * Creates a new User in user cache for given account Identifier.
     * @param accountIdentifier
     */
    void createUserInUserCache(String accountIdentifier, User user);

    /**
     * Remove user details for given accountId from user cache.
     *
     * @param accountIdentifier
     */
    void removeUserFromDatasource(String accountIdentifier);

//    /**
//     * Check if request is from registered marketPlace
//     * @param openId
//     * @return true is from registered market place and false if from other sites.
//     */
//    boolean isrequestFromValidMarketPlace(String openId);
}
