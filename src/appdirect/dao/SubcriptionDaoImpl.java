package appdirect.dao;

import javax.annotation.Resource;

import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.stereotype.Repository;

import appdirect.common.AppConstants;
import appdirect.model.Account;
import appdirect.model.EventResponse;
import appdirect.model.SubscriptionEvent;

@Repository
public class SubcriptionDaoImpl implements SubcriptionDao {

    @Resource(name = "cacheManager")
    private EhCacheCacheManager cacheManager;

    @Override
    public EventResponse saveSubscriptionEventDetais(SubscriptionEvent eventDetails) {
        Cache cache = getCacheManager().getCache(AppConstants.SUBSCRIPTION_EVENT_CACHE);
        Account account = new Account();
        String accountIdentifier = generateId(AppConstants.PREFIX_FOR_ACC_ID);
        account.setAccountIdentifier(accountIdentifier);
        account.setStatus(AppConstants.ACC_STATUS_ACTIVE);
        eventDetails.setAccount(account);
        cache.put(accountIdentifier, eventDetails);
        return createAndReturnSuccessResponse(accountIdentifier, AppConstants.SUBS_ORDER_SUCCESS);
    }

    @Override
    public EventResponse updateSubscriptionEventDetais(SubscriptionEvent eventDetails, String accountIdentifier) {
        Cache cache = getCacheManager().getCache(AppConstants.SUBSCRIPTION_EVENT_CACHE);
        cache.put(accountIdentifier, eventDetails);
        return createAndReturnSuccessResponse(accountIdentifier, AppConstants.SUBS_CHANGE_SUCCESS);
    }

    @Override
    public EventResponse cancelSubscriptionEventDetais(String accIdentifier) {
        Cache cache = getCacheManager().getCache(AppConstants.SUBSCRIPTION_EVENT_CACHE);
        cache.evict(accIdentifier);
        return createAndReturnSuccessResponse(accIdentifier, AppConstants.SUBS_CANCELED_SUCCESS);
    }
    
    // This is just for Reference
    @Override
    public SubscriptionEvent fetchEventInfoFromAccId(String accountIdentifier) {
        Cache cache = getCacheManager().getCache(AppConstants.SUBSCRIPTION_EVENT_CACHE);
        SubscriptionEvent event = (SubscriptionEvent) cache.get(accountIdentifier).get();
        return event;
    }

    @Override
    public ValueWrapper fetchValueWrapperForEvent(String cacheName, String accIdentifier) {
        Cache cache = getCacheManager().getCache(cacheName);
        ValueWrapper valueWrapper = cache.get(accIdentifier);
        return valueWrapper;
    }

    public EhCacheCacheManager getCacheManager() {
        return cacheManager;
    }
    
    public void setCacheManager(EhCacheCacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }
    
    // Used to create uniqueId for Subscription Event.
    private String generateId(String prefix) {
        double randomNumber = Math.random() * 999999999;
        return prefix + (System.currentTimeMillis() / 1000) + ((int) randomNumber);
    }

    private EventResponse createAndReturnSuccessResponse(String accIdentifier, String successMsg) {
        EventResponse response = new EventResponse();
        response.setSuccess(true);
        response.setAccountIdentifier(accIdentifier);
        return response;
    }
}