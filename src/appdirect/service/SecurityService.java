package appdirect.service;

import org.openid4java.consumer.ConsumerException;
import org.openid4java.discovery.DiscoveryException;
import org.openid4java.message.MessageException;
import org.springframework.stereotype.Service;

import appdirect.model.SubscriptionEvent;

/**
 * This service serves application level requests.
 * 
 * @author apurv
 */
@Service
public interface SecurityService {

    /**
     * Creates User object which is passed to DAO to be stored in user cache.
     * 
     * @param email
     * @param openId
     */
    void saveUserInfo(String email, String openId);

    /**
     * @param openId
     * @param accountId 
     * @return OpenId login URL which is used to redirect to openId Provider for authentication
     * @throws DiscoveryException
     * @throws MessageException
     * @throws ConsumerException
     */
    String fetchOpenIdLoginURL(String openId, String accountId) throws DiscoveryException, MessageException, ConsumerException;

    /**
     * Gets openId from openIdURL
     * 
     * @param openId
     * @return
     */
    String generateAccountIdentifier(String openId);

    /**
     * Check if given user exists in application datastrore.
     * 
     * @param userId
     * @return true is user exists and false if user doesn't exist.
     */
    boolean isAnExistingUserInDatastore(String userId);

    /**
     * Check if request is from registered source
     * @param accountId 
     * @param openId
     * @return true is from registered market place and false if from other sites.
     */
    boolean isrequestFromValidSource(String openId, String accountId);

    /**
     * Check if the marketplace from where the request is made is valid.
     * @param openId
     * @return true if market place is valid
     */
    boolean isMarketplaceValid(String openId);

    /**
     * Creates user in user cache for given accountIdentifier and extracting User info from subscription Event.
     * @param accountIdentifier
     * @param subscriptionEvent 
     */
    void createUserInUserCache(String accountIdentifier, SubscriptionEvent subscriptionEvent);

    /**
     * Remove the User details for given accountIf from user cache.
     * @param accountIdentifier
     */
    void removeUserFromCache(String accountIdentifier);

}
