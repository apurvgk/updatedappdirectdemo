package appdirect.service;

import org.springframework.stereotype.Service;

import appdirect.common.ErrorCodes;
import appdirect.model.EventResponse;
import appdirect.model.SubscriptionEvent;

/**
 * This service serves requests related to Subscriptions from App direct.
 * 
 * @author apurv
 */
@Service
public interface SubscriptionService {

    /**
     * Save the SubscriptionEvent in subscription events cache.
     * 
     * @param subscriptionEvent
     * @return EventResponse
     */
    public EventResponse saveSubscriptionEventDetais(SubscriptionEvent subscriptionEvent);

    /**
     * Update the subscription event details in events cache
     * 
     * @param subscriptionEvent
     * @return
     */
    public EventResponse updateSubscriptionEventDetais(SubscriptionEvent subscriptionEvent);

    /**
     * Used to remove subscription event details from events cache.
     * 
     * @param url
     * @return
     */
    public EventResponse cancelSubscriptionEventDetais(String url);

    /**
     * Fetch the SubscriptionEvent from cache for given account Id.
     * 
     * @param accountIdentifier
     * @return
     */
    public SubscriptionEvent fetchEventInfoFromAccId(String accountIdentifier);

    /**
     * Fetch the required Subscription Event for input URL
     * 
     * @param url
     * @return Subscription Event
     */
    SubscriptionEvent fetchSubscriptionEventDetails(String url);

    /**
     * Create an EventResponse Object for error response with specific error code during subscription event.
     * 
     * @param accIdentifier
     * @param errorCode
     * @return EventResponse
     */
    EventResponse createAndReturnErrorResponse(String accIdentifier, ErrorCodes errorCode);
}
