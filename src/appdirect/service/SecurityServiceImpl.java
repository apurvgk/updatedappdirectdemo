package appdirect.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.openid4java.consumer.ConsumerException;
import org.openid4java.consumer.ConsumerManager;
import org.openid4java.discovery.DiscoveryException;
import org.openid4java.discovery.DiscoveryInformation;
import org.openid4java.message.AuthRequest;
import org.openid4java.message.MessageException;
import org.openid4java.message.sreg.SRegRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import appdirect.common.AppConstants;
import appdirect.common.ConditionalUtil;
import appdirect.dao.SecurityDao;
import appdirect.model.Creator;
import appdirect.model.MarketPlaces;
import appdirect.model.SubscriptionEvent;
import appdirect.model.User;

@Service
public class SecurityServiceImpl implements SecurityService {

    private static Logger LOGGER = Logger.getLogger("applicationServiceImpl");

    @Autowired
    private SecurityDao dao;

    @Value("${application.home.url}")
    private String applicationHomeURL;

    @Override
    public void saveUserInfo(String email, String openId) {
        String accountIdentifier = generateAccountIdentifier(openId);
        if (!getDao().isAnExistingUserInDatastore(accountIdentifier)) {
            User user = new User();
            user.setEmail(email);
            user.setOpenId(openId);
            getDao().saveUserInfo(user, accountIdentifier);
            LOGGER.debug("Saved userInfo for user with email: " + email + " and openId: " + openId);
        }
    }

    @Override
    public String fetchOpenIdLoginURL(String openidUrl, String accountId) throws DiscoveryException, MessageException, ConsumerException {
        ConsumerManager manager = new ConsumerManager();
        List discoveryList = manager.discover(openidUrl);
        DiscoveryInformation discovery = manager.associate(discoveryList);
        String redirectURL = createRedirectURL(accountId);
        AuthRequest authReq = manager.authenticate(discovery, redirectURL);
        SRegRequest sRegRequest = createSRegRequest();
        authReq.addExtension(sRegRequest);
        String destinationUrl = authReq.getDestinationUrl(true);
        return destinationUrl;
    }

    private String createRedirectURL(String accountId) {
        return getApplicationHomeURL().replace(AppConstants.ACC_ID_REPLACE_STRING, accountId);
    }

    @Override
    public boolean isrequestFromValidSource(String openId, String accountId) {
        if (ConditionalUtil.isNullOrEmpty(accountId)) {
            return false;
        }
        return isMarketplaceValid(openId);
    }

    @Override
    public boolean isMarketplaceValid(String inputURL) {
        for (MarketPlaces marketPlace : MarketPlaces.values()) {
            if (!ConditionalUtil.isNullOrEmpty(inputURL) && inputURL.contains(marketPlace.getDomainName())) {
                return true;
            }
        }
        return false;
    }

    public SecurityDao getDao() {
        return dao;
    }

    public void setDao(SecurityDao dao) {
        this.dao = dao;
    }

    public String generateAccountIdentifier(String openId) {
        String[] splitArray = openId.split(AppConstants.OPEN_ID_CODE_REGEX);
        return splitArray[1];
    }

    private SRegRequest createSRegRequest() {
        SRegRequest sRegRequest = SRegRequest.createFetchRequest();
        sRegRequest.addAttribute("email", true);
        sRegRequest.addAttribute("fullname", true);
        sRegRequest.addAttribute("nickname", true);
        sRegRequest.addAttribute("postcode", true);
        return sRegRequest;
    }

    public String getApplicationHomeURL() {
        return applicationHomeURL;
    }

    public void setApplicationHomeURL(String applicationHomeURL) {
        this.applicationHomeURL = applicationHomeURL;
    }

    @Override
    public boolean isAnExistingUserInDatastore(String userId) {
        if (ConditionalUtil.isNullOrEmpty(userId)) {
            return false;
        }
        return getDao().isAnExistingUserInDatastore(userId);
    }

    @Override
    public void createUserInUserCache(String accountIdentifier, SubscriptionEvent subscriptionEvent) {
        User user = createUserFromEvent(subscriptionEvent);
        getDao().createUserInUserCache(accountIdentifier, user);
    }

    private User createUserFromEvent(SubscriptionEvent subscriptionEvent) {
        User user = new User();
        Creator creator = subscriptionEvent.getCreator();
        if (null != creator) {
            user.setEmail(creator.getEmail());
            user.setOpenId(creator.getOpenId());
            user.setFirstName(creator.getFirstName());
            user.setLastName(creator.getLastName());
            user.setLanguage(creator.getLanguage());
            user.setLocale(creator.getLocale());
            user.setUuid(creator.getUuid());
        }
        return user;
    }

    @Override
    public void removeUserFromCache(String accountIdentifier) {
        if (ConditionalUtil.isNullOrEmpty(accountIdentifier)) {
            LOGGER.debug("Cannot delete record from user cache for account id: " + accountIdentifier);
        }
        else {
            getDao().removeUserFromDatasource(accountIdentifier);
        }
    }
}