package appdirect.service;

import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import appdirect.common.AppConstants;
import appdirect.common.ErrorCodes;
import appdirect.dao.SubcriptionDao;
import appdirect.model.EventResponse;
import appdirect.model.SubscriptionEvent;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.signature.QueryStringSigningStrategy;

@Service
@PropertySource("classpath:appDirect.properties")
public class SubcriptionServiceImpl implements SubscriptionService {

    private static Logger LOGGER = Logger.getLogger("subcriptionServiceImpl");

    @Autowired
    private SubcriptionDao dao;

    @Value("${consumer.token.id}")
    private String consumerTokenId;

    @Value("${consumer.token.secret}")
    private String consumerTokenSecret;

    @Override
    public EventResponse saveSubscriptionEventDetais(SubscriptionEvent subscriptionEvent) {
        String accountIdentifier = extractAccountIdentifier(subscriptionEvent);
        ValueWrapper valueWrapper = getDao().fetchValueWrapperForEvent(AppConstants.SUBSCRIPTION_EVENT_CACHE, accountIdentifier);
        if (null != valueWrapper) {
            LOGGER.debug(ErrorCodes.USER_ALREADY_EXISTS);
            return createAndReturnErrorResponse(accountIdentifier, ErrorCodes.USER_ALREADY_EXISTS);
        }
        LOGGER.debug("Saving subsciprtion Event in event cache");
        return getDao().saveSubscriptionEventDetais(subscriptionEvent);
    }

    @Override
    public EventResponse updateSubscriptionEventDetais(SubscriptionEvent subscriptionEvent) {
        String accIdentifier = extractAccountIdentifier(subscriptionEvent);
        ValueWrapper valueWrapper = getDao().fetchValueWrapperForEvent(AppConstants.SUBSCRIPTION_EVENT_CACHE, accIdentifier);
        if (null == valueWrapper) {
            return createAndReturnErrorResponse(accIdentifier, ErrorCodes.ACCOUNT_NOT_FOUND);
        }
        return getDao().updateSubscriptionEventDetais(subscriptionEvent, accIdentifier);
    }

    @Override
    public EventResponse cancelSubscriptionEventDetais(String url) {
        SubscriptionEvent subscriptionEvent = fetchSubscriptionEventDetails(url);
        String accIdentifier = extractAccountIdentifier(subscriptionEvent);
        ValueWrapper valueWrapper = getDao().fetchValueWrapperForEvent(AppConstants.SUBSCRIPTION_EVENT_CACHE, accIdentifier);
        if (null == valueWrapper) {
            return createAndReturnErrorResponse(accIdentifier, ErrorCodes.ACCOUNT_NOT_FOUND);
        }
        return getDao().cancelSubscriptionEventDetais(accIdentifier);
    }

    @Override
    public SubscriptionEvent fetchEventInfoFromAccId(String accountIdentifier) {
        ValueWrapper valueWrapper = getDao().fetchValueWrapperForEvent(AppConstants.SUBSCRIPTION_EVENT_CACHE, accountIdentifier);
        if (null == valueWrapper) {
            return new SubscriptionEvent();
        }
        return getDao().fetchEventInfoFromAccId(accountIdentifier);
    }

    public SubcriptionDao getDao() {
        return dao;
    }

    public void setDao(SubcriptionDao dao) {
        this.dao = dao;
    }

    /**
     * @param url
     * @return SubscriptionEvent Object from the JSON response returned by AppDirect service
     */
    @Override
    public SubscriptionEvent fetchSubscriptionEventDetails(String url) {
        RestTemplate restTemplate = new RestTemplate();
        OAuthConsumer consumer = new DefaultOAuthConsumer(getConsumerTokenId(), getConsumerTokenSecret());
        consumer.setSigningStrategy(new QueryStringSigningStrategy());
        SubscriptionEvent subscriptionEvent = new SubscriptionEvent();
        try {
            String signedUrl = consumer.sign(url);
            HttpHeaders headers = createHeaders();
            HttpEntity<String> entity = new HttpEntity<String>(headers);
            ResponseEntity<SubscriptionEvent> response = restTemplate.exchange(signedUrl, HttpMethod.GET, entity, SubscriptionEvent.class);
            subscriptionEvent = response.getBody();

        }
        catch (RestClientException | OAuthMessageSignerException | OAuthExpectationFailedException
            | OAuthCommunicationException e) {
            LOGGER.debug(e.getMessage());
            LOGGER.error(e.getStackTrace());
        }
        return subscriptionEvent;
    }

    private HttpHeaders createHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.ACCEPT, "text/html,application/xhtml+xml,application/xml");
        return headers;
    }

    private String extractAccountIdentifier(SubscriptionEvent eventDetails) {
        String accIdentifier = null != eventDetails.getAccount() ? eventDetails.getAccount().getAccountIdentifier() : null;
        return accIdentifier;
    }

    @Override
    public EventResponse createAndReturnErrorResponse(String accIdentifier, ErrorCodes errorCode) {
        EventResponse response = new EventResponse();
        response.setAccountIdentifier(accIdentifier);
        response.setErrorCode(errorCode.toString());
        response.setMessage(errorCode.getMessage());
        response.setSuccess(false);
        return response;
    }

    public String getConsumerTokenId() {
        return consumerTokenId;
    }

    public void setConsumerTokenId(String consumerTokenId) {
        this.consumerTokenId = consumerTokenId;
    }

    public String getConsumerTokenSecret() {
        return consumerTokenSecret;
    }

    public void setConsumerTokenSecret(String consumerTokenSecret) {
        this.consumerTokenSecret = consumerTokenSecret;
    }
}