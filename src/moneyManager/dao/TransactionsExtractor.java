package moneyManager.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import moneyManager.models.Transaction;
import moneyManager.models.Transactions;

public class TransactionsExtractor implements ResultSetExtractor<Transactions> {

	@Override
	public Transactions extractData(ResultSet rs) throws SQLException,
			DataAccessException {
		Transactions transactions = new Transactions();
		while (rs.next()) {
			Transaction transaction = new Transaction();
			transaction.setTransactionId(rs.getString("TRANSACTION_ID"));
			transaction.setDescription(rs.getString("DESCRIPTION"));
			transaction.setTitle(rs.getString("TITLE"));
			transaction.setInitiator(rs.getString("INITIATOR"));
			transaction.setRecipient(rs.getString("RECIPIENT"));
			transaction.setAmount(rs.getDouble("AMOUNT"));
			transaction.setCreationDate(rs.getString("CREATION_DATE"));
			transaction.setModificationDate(rs.getString("MODIFICATION_DATE"));
			transaction.setStatus(rs.getString("STATUS"));
			transaction.setType(rs.getString("TYPE"));
            transaction.setUserId(rs.getString("USER_ID"));
			transactions.addTransaction(transaction);
		}
		return transactions;
	}
}