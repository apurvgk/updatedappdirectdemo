package moneyManager.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import moneyManager.models.Transaction;

public class TransactionMapper implements RowMapper<Transaction> {
	
	@Override
	public Transaction mapRow(ResultSet rs, int rowNumber) throws SQLException {
		Transaction transaction = new Transaction();
		transaction.setTransactionId(rs.getString("TRANSACTION_ID"));
		transaction.setDescription(rs.getString("DESCRIPTION"));
		transaction.setTitle(rs.getString("TITLE"));
		transaction.setInitiator(rs.getString("INITIATOR"));
		transaction.setRecipient(rs.getString("RECIPIENT"));
		transaction.setAmount(rs.getDouble("AMOUNT"));
		transaction.setCreationDate(rs.getString("CREATION_DATE"));
		transaction.setModificationDate(rs.getString("MODIFICATION_DATE"));
		transaction.setStatus(rs.getString("STATUS"));	
		transaction.setType(rs.getString("TYPE"));	
		return transaction;
	}
}
