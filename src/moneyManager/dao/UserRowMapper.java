package moneyManager.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import moneyManager.models.User;

public class UserRowMapper implements RowMapper<User> {
	
	@Override
    public User mapRow(ResultSet rs, int rowNumber) throws SQLException {
        User user = new User();
        user.setUserId(rs.getString("USER_ID"));
        user.setFirstName(rs.getString("FIRST_NAME"));
        user.setLastName(rs.getString("LAST_NAME"));
        user.setEmailId(rs.getString("EMAIL_ID"));
        user.setPhoneNumber(rs.getString("PHONE_NUMBER"));
        return user;
	}
}
