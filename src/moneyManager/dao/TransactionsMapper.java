package moneyManager.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import moneyManager.models.Transaction;
import moneyManager.models.Transactions;

public class TransactionsMapper implements RowMapper<Transactions> {

	@Override
	public Transactions mapRow(ResultSet rs, int rowNumber) throws SQLException {
		Transactions transactions = new Transactions();
		// for (int i = 0; i < rowNumber; i++) {
		Transaction transaction = new Transaction();
		transaction.setTransactionId(rs.getString("TRANSACTION_ID"));
		transaction.setDescription(rs.getString("DESCRIPTION"));
		transaction.setTitle(rs.getString("TITLE"));
		transaction.setInitiator(rs.getString("INITIATOR"));
		transaction.setRecipient(rs.getString("RECIPIENT"));
		transaction.setAmount(rs.getDouble("AMOUNT"));
		transaction.setCreationDate(rs.getString("CREATION_DATE"));
		transaction.setModificationDate(rs.getString("MODIFICATION_DATE"));
		transaction.setStatus(rs.getString("STATUS"));	
		transaction.setType(rs.getString("TYPE"));	
		transactions.addTransaction(transaction);
		// }
		return transactions;
	}
}