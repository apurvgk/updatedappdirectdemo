/**
 * 
 */
package moneyManager.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import moneyManager.models.AddEditTransactionForm;
import moneyManager.models.Transaction;
import moneyManager.models.Transactions;
import moneyManager.models.User;

/**
 * @author Admin
 *
 */
@Component
public interface MoneyTransactionDao {

	Transaction fetchTransaction();

	Transactions fetchTransactions(String userId);

	Transactions saveTransactions(AddEditTransactionForm addEditForm);

    Transactions saveUserInfo(User user);

    Transactions fetchTransactionDetails(String transactionId);

	Transactions updateTransaction(Transaction transaction);

    List<User> fetchUserDetailsFromId(User user);

    Transactions saveTransaction(Transaction transaction);

    Transactions fetchAllTransactionsForUser(String userId);

    Transactions completeTransaction(String userId, String transactionId);

}
