/**
 * 
 */
package moneyManager.dao;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import moneyManager.models.AddEditTransactionForm;
import moneyManager.models.Transaction;
import moneyManager.models.Transactions;
import moneyManager.models.User;

/**
 * @author ApurvKomerwar
 */
@Component
public class MoneyTransactionMysqlDaoImpl implements MoneyTransactionDao {

    private static Logger LOGGER = Logger.getLogger("MoneyTransactionMysqlDaoImpl");

    @Resource
    private JdbcTemplate jdbcTemplateObject;

    public JdbcTemplate getJdbcTemplateObject() {
        return jdbcTemplateObject;
    }

    public void setJdbcTemplateObject(JdbcTemplate jdbcTemplateObject) {
        this.jdbcTemplateObject = jdbcTemplateObject;
    }

    @Override
    public Transaction fetchTransaction() {
        LOGGER.debug("Getting all transactions");
        String SQL = "select * from transactions";
        Transaction student = getJdbcTemplateObject().queryForObject(SQL, null, new TransactionMapper());
        return student;
    }

    @Override
    public Transactions fetchTransactions(String userId) {
        LOGGER.debug("Fetching transactions for user: " + userId);
        String SQL = MySQLStatements.FETCH_TRANSACTIONS_FOR_USER_SQL;
        Transactions transactions = getJdbcTemplateObject().query(SQL, new TransactionsExtractor(), userId);
        return transactions;
    }

    @Override
    public Transactions saveTransactions(AddEditTransactionForm addEditForm) {
        String userId = addEditForm.getUserId();
        String transactionId = createTransactionId(addEditForm);
        LOGGER.debug("Saving transaction for user: " + userId + " with transactionId: " + transactionId);
        getJdbcTemplateObject().update(MySQLStatements.INSERT_INTO_UTM_SQL, userId, transactionId, userId);
        getJdbcTemplateObject().update(MySQLStatements.INSERT_INTO_TRANSACTIONS_SQL, transactionId, addEditForm.getTitle(), userId, addEditForm.getName(), addEditForm.getAmount(), addEditForm.getTransactionType().getType());
        return fetchTransactions(userId);
    }

    private String createTransactionId(AddEditTransactionForm addEditForm) {

        return addEditForm.getTitle() + addEditForm.getName();
    }

    @Override
    public Transactions saveUserInfo(User user) {
        String userId = user.getUserId();
        LOGGER.debug("Saving user Info for user: " + userId);
        getJdbcTemplateObject().update(MySQLStatements.SAVE_USER_INFO, userId, user.getFirstName(), user.getLastName(), user.getEmailId(), user.getPhoneNumber());
        return fetchTransactions(userId);
    }

    @Override
    public Transactions fetchTransactionDetails(String transactionId) {
        Transactions transactions = getJdbcTemplateObject().query(MySQLStatements.FETCH_TRANSACTION_FOR_TRANSACTION_ID, new TransactionsExtractor(), transactionId);
        return transactions;
    }

    @Override
    public Transactions fetchAllTransactionsForUser(String userId) {
        Transactions transactions = getJdbcTemplateObject().query(MySQLStatements.FETCH_TRANSACTIONS_FOR_USER_SQL, new TransactionsExtractor(), userId);
        return transactions;
    }

    @Override
    public Transactions updateTransaction(Transaction transaction) {
        String userId = transaction.getUserId();
        LOGGER.debug("updating transaction for user: " + userId + " with transactionId: " + transaction.getTransactionId());
        getJdbcTemplateObject().update(MySQLStatements.UPDATE_TRANSACTION, transaction.getTitle(), transaction.getInitiator(), transaction.getRecipient(), transaction.getAmount(), transaction.getDescription(), transaction.getStatus(), transaction.getType(), transaction.getTransactionId());
        return fetchTransactions(userId);
    }

    @Override
    public List<User> fetchUserDetailsFromId(User user) {
        List<User> queryResult = getJdbcTemplateObject().query(MySQLStatements.FETCH_USER_DETAILS_FORM_ID, new UserRowMapper(), user.getUserId());
        return queryResult;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Transactions saveTransaction(Transaction transaction) {
        getJdbcTemplateObject().update(MySQLStatements.INSERT_NEW_TRANSACTION, transaction.getTransactionId(), transaction.getDescription(), transaction.getTitle(), transaction.getInitiator(), transaction.getRecipient(), transaction.getAmount(), transaction.getStatus(), transaction.getType());
        getJdbcTemplateObject().update(MySQLStatements.INSERT_NEW_UTM, transaction.getUserId(), transaction.getTransactionId());
        return getJdbcTemplateObject().query(MySQLStatements.FETCH_TRANSACTIONS_FOR_USER_SQL, new TransactionsExtractor(), transaction.getUserId());
    }

    @Override
    public Transactions completeTransaction(String userId, String transactionId) {
        LOGGER.debug("Completing transaction for user: " + userId + " with transactionId: " + transactionId);
        getJdbcTemplateObject().update(MySQLStatements.COMPLETE_TRANSACTION, transactionId);
        return fetchTransactions(userId);
    }
}