package moneyManager.controller;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import appdirect.common.AppConstants;
import appdirect.service.SecurityService;
import moneyManager.models.AddEditTransactionForm;
import moneyManager.models.HomeForm;
import moneyManager.models.Transaction;
import moneyManager.models.TransactionType;
import moneyManager.models.Transactions;
import moneyManager.models.User;
import moneyManager.service.MoneyTransactionService;

/**
 * MoneyManager application controller. It contains all the API's to perform operation on moneyManager.
 * 
 * @author apurv
 */
@Controller
public class HomeController {

    private static Logger LOGGER = Logger.getLogger("homeController");

    @Resource(name = "moneyTransactionServiceImpl")
    private MoneyTransactionService moneyTransactionService;

    @Resource(name = "securityServiceImpl")
    private SecurityService securityService;

    @ModelAttribute("homeForm")
    public HomeForm construct() {
        return new HomeForm();
    }

    @ModelAttribute("addEditForm")
    public AddEditTransactionForm constructAddEditTransactionForm() {
        return new AddEditTransactionForm();
    }

    @ModelAttribute("user")
    public User constructUser() {
        return new User();
    }

    public MoneyTransactionService getMoneyTransactionService() {
        return moneyTransactionService;
    }

    public void setMoneyTransactionService(MoneyTransactionService moneyTransactionService) {
        this.moneyTransactionService = moneyTransactionService;
    }

    public SecurityService getSecurityService() {
        return securityService;
    }

    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    @RequestMapping(value = {"/home"}, method = RequestMethod.GET)
    public String viewTransaction(@ModelAttribute("homeForm") HomeForm homeForm, final Model model) {
        return "home";
    }

    @RequestMapping(value = {"/saveUserInfo"}, method = RequestMethod.POST)
    public String saveUserInfo(@ModelAttribute("user") User user, final Model model) {
        Transactions transactions = getMoneyTransactionService().saveUserInfo(user);
        model.addAttribute("transactions", transactions.getTransactions());
        model.addAttribute("transactionsWrapper", transactions);
        transactions.setUserId(user.getUserId());
        return "viewDetails";
    }

    @RequestMapping(value = {"/viewTransactions"}, method = RequestMethod.GET)
    public String viewUserTransactions(@ModelAttribute("userId") String userId, final Model model) {
        boolean userExists = getSecurityService().isAnExistingUserInDatastore(userId);
        if (userExists) {
            Transactions transactions = getMoneyTransactionService().fetchTransactions(userId);
            model.addAttribute("transactions", transactions.getTransactions());
            return "viewDetails";
        }
        LOGGER.debug("Application access denied as user with accountId: " + userId + " does not exists");
        return AppConstants.ACCESS_DENIED_JSP;
    }

    @RequestMapping(value = {"/viewTransactionsFromDetails"}, method = RequestMethod.GET)
    public String viewTransactionsFromDetails(final Model model, @RequestParam("userId") String userId) {
        Transactions transactions = getMoneyTransactionService().fetchTransactions(userId);
        model.addAttribute("transactions", transactions.getTransactions());
        return "viewDetails";
    }

    @RequestMapping(value = {"/viewTransactionDetails"}, method = RequestMethod.GET)
    public String viewTransactionDetails(final Model model, @RequestParam("userId") String userId, @RequestParam("transactionId") String transactionId) {
        Transactions transactions = getMoneyTransactionService().fetchTransactionDetails(transactionId);
        Transaction transaction = transactions.getTransactions().get(0);
        model.addAttribute("transaction", transaction);
        return "viewTransactionDetails";
    }

    @RequestMapping(value = {"/addEditTransactions"}, method = RequestMethod.GET)
    public String addEditTransactions(@ModelAttribute("transaction") Transaction transaction, final Model model, @RequestParam("userId") String userId) {
        List<TransactionType> typeList = Arrays.asList(TransactionType.values());
        model.addAttribute("userId", userId);
        model.addAttribute("typeList", typeList);
        return "addEdit";
    }

    @RequestMapping(value = {"/saveTransactions"}, method = RequestMethod.POST)
    public String saveTransactions(

        @ModelAttribute("transaction") Transaction transaction, final Model model) {
        Transactions transactions = getMoneyTransactionService().saveTransaction(transaction);
        model.addAttribute("transactions", transactions.getTransactions());
        if (null == transactions.getTransactions() || transactions.getTransactions().isEmpty()) {
            return "home";
        }
        return "viewDetails";
    }

    @RequestMapping(value = {"/updateTransaction"}, method = RequestMethod.POST)
    public String updateTransaction(@ModelAttribute("transaction") Transaction transaction, final Model model) {
        Transactions transactions = getMoneyTransactionService().updateTransaction(transaction);
        model.addAttribute("transactions", transactions.getTransactions());
        return "viewDetails";
    }

    // To redirect to edit screen from viewDetais and viewTransactionDetails screen
    @RequestMapping(value = {"/editTransaction"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String editTransaction(final Model model, @RequestParam("transactionId") String transactionId) {
        Transactions transactions = getMoneyTransactionService().fetchTransactionDetails(transactionId);
        Transaction transaction = transactions.getTransaction(0);
        model.addAttribute("transaction", transaction);
        return "editTransaction";
    }

    // To Complete transaction from viewDetails screen
    @RequestMapping(value = {"/completeTransaction"}, method = RequestMethod.GET)
    public String completeTransaction(final Model model, @RequestParam("userId") String userId, @RequestParam("transactionId") String transactionId) {
        Transactions transactions = getMoneyTransactionService().completeTransaction(userId, transactionId);
        model.addAttribute("transactions", transactions.getTransactions());
        return "viewDetails";
    }

    // The login directly to MoneyManager is not yet supported, we will have to make request from Appdirect marketplace
    // in order to access the application
    @RequestMapping(value = {"/formLogin.do"}, method = RequestMethod.POST)
    public String formLogin(@ModelAttribute("user") User user, final Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        /**
         * TODO if(uName and Pass does not match in DB){ redirect to registration page. } else{ Below mention code will
         * follow. }
         */
        if (getMoneyTransactionService().isUserInformationNotAvailable(user)) {
            return "userInfo";
        }
        Transactions transactions = getMoneyTransactionService().fetchTransactions(user.getUserId());
        model.addAttribute("transactions", transactions.getTransactions());
        model.addAttribute("transactionsWrapper", transactions);
        transactions.setUserId(user.getUserId());
        return "viewDetails";
    }
}