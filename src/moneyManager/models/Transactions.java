package moneyManager.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class Transactions implements Iterable<Transaction> {
	
	private List<Transaction> transactions = new ArrayList<>();

	private String userId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}
	
	public void addTransaction(Transaction transaction){
		transactions.add(transaction);
	}
	
	public Transaction getTransaction(int index){
		return transactions.get(index);
	}
	
	@Override
	public Iterator<Transaction> iterator() {
		return transactions.iterator();
	}
}