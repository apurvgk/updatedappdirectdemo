package moneyManager.models;

import java.util.Arrays;
import java.util.List;

public enum TransactionType {

    GIVE("GIVE"),
    TAKE("TAKE");

    private String type;

    private List<TransactionType> valueList;

    private TransactionType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public List<TransactionType> getValues() {
        this.valueList = Arrays.asList(TransactionType.values());
        return valueList;
    }
}