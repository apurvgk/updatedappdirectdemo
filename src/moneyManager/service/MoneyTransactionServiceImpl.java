package moneyManager.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import appdirect.common.ConditionalUtil;
import moneyManager.dao.MoneyTransactionDao;
import moneyManager.models.AddEditTransactionForm;
import moneyManager.models.Transaction;
import moneyManager.models.Transactions;
import moneyManager.models.User;

@Component("moneyTransactionServiceImpl")
public class MoneyTransactionServiceImpl implements MoneyTransactionService {

    @Resource(name = "moneyTransactionMysqlDaoImpl")
    private MoneyTransactionDao moneyTransactionDao;

    @Override
    public Transaction fetchTransaction() {
        return moneyTransactionDao.fetchTransaction();
    }

    @Override
    public Transactions fetchTransactions(String userId) {
        return moneyTransactionDao.fetchTransactions(userId);
    }

    @Override
    public Transactions saveTransactions(AddEditTransactionForm addEditForm) {
        return moneyTransactionDao.saveTransactions(addEditForm);
    }

    @Override
    public Transactions saveUserInfo(User User) {
        return moneyTransactionDao.saveUserInfo(User);
    }

    @Override
    public Transactions fetchTransactionDetails(String transactionId) {
        return moneyTransactionDao.fetchTransactionDetails(transactionId);
    }

    @Override
    public Transactions updateTransaction(Transaction transaction) {
        return moneyTransactionDao.updateTransaction(transaction);
    }

    @Override
    public boolean isUserInformationNotAvailable(User user) {
        List<User> userFromDb = moneyTransactionDao.fetchUserDetailsFromId(user);
        if (ConditionalUtil.isNullOrEmpty(userFromDb)) {
            return true;
        }
        return ConditionalUtil.isNullOrEmpty(userFromDb.get(0).getFirstName()) || ConditionalUtil.isNullOrEmpty(userFromDb.get(0).getEmailId()) || ConditionalUtil.isNullOrEmpty(userFromDb.get(0).getLastName()) || ConditionalUtil.isNullOrEmpty(userFromDb.get(0).getPhoneNumber());
    }

    @Override
    public Transactions saveTransaction(Transaction transaction) {
        String transactionId = generateTransactionId();
        transaction.setTransactionId(transactionId);
        return moneyTransactionDao.saveTransaction(transaction);
    }

    private String generateTransactionId() {
        double randomNumber = Math.random() * 999999999;
        return "TRXN" + (System.currentTimeMillis() / 1000) + ((int) randomNumber);
    }

    @Override
    public Transactions fetchAllTransactionsForUser(String userId) {
        return moneyTransactionDao.fetchAllTransactionsForUser(userId);
    }

    @Override
    public Transactions completeTransaction(String userId, String transactionId) {
        return moneyTransactionDao.completeTransaction(userId, transactionId);
    }
}