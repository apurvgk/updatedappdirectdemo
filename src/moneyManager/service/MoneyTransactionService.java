package moneyManager.service;

import org.springframework.stereotype.Service;

import moneyManager.models.AddEditTransactionForm;
import moneyManager.models.Transaction;
import moneyManager.models.Transactions;
import moneyManager.models.User;

@Service
public interface MoneyTransactionService {

	Transaction fetchTransaction();

	Transactions fetchTransactions(String userId);

	Transactions saveTransactions(AddEditTransactionForm addEditForm);

    Transactions saveUserInfo(User user);

    Transactions fetchTransactionDetails(String transactionId);

	Transactions updateTransaction(Transaction transaction);

    boolean isUserInformationNotAvailable(User user);

    Transactions saveTransaction(Transaction transaction);

    Transactions fetchAllTransactionsForUser(String userId);

    Transactions completeTransaction(String transactionId, String transactionId2);
}