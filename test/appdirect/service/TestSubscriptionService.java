package appdirect.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.support.SimpleValueWrapper;
import org.springframework.http.HttpStatus;

import appdirect.common.AppConstants;
import appdirect.common.ErrorCodes;
import appdirect.dao.SubcriptionDaoImpl;
import appdirect.model.Account;
import appdirect.model.Creator;
import appdirect.model.EventResponse;
import appdirect.model.SubscriptionEvent;
import appdirect.service.SubcriptionServiceImpl;
import junit.framework.Assert;
/**
 * Note: Due to time constraint only two test cases are written.
 * @author apurv
 *
 */
public class TestSubscriptionService {

    private static final String TEST_NAME = "TestName";

    private static final String TEST_ACC_ID = "TestAccId";

    private static final String TEST_URL = "testUrl";

    private SubcriptionServiceImpl subcriptionService;

    private SubcriptionDaoImpl dao;

    private EhCacheCacheManager cacheManager;

    @Before
    public void beforeTestsRun() {
        dao = Mockito.mock(SubcriptionDaoImpl.class);
        cacheManager = Mockito.mock(EhCacheCacheManager.class);
        dao.setCacheManager(cacheManager);
        subcriptionService = Mockito.mock(SubcriptionServiceImpl.class);
        subcriptionService.setDao(dao);
    }

    @Test
    public void testSaveSubscriptionEventDetaisForError() {
        SubscriptionEvent subscriptionEvent = new SubscriptionEvent();
        Account account = new Account();
        account.setAccountIdentifier(TEST_ACC_ID);
        subscriptionEvent.setAccount(account);
        Creator creator = new Creator();
        creator.setFirstName(TEST_NAME);
        subscriptionEvent.setCreator(creator);
        Mockito.when(subcriptionService.fetchSubscriptionEventDetails(TEST_URL)).thenReturn(subscriptionEvent);
        Mockito.when(subcriptionService.getDao()).thenReturn(dao);
        Mockito.when(subcriptionService.getDao().fetchValueWrapperForEvent(AppConstants.SUBSCRIPTION_EVENT_CACHE, TEST_ACC_ID)).thenReturn(new SimpleValueWrapper(subscriptionEvent));
        Mockito.when(subcriptionService.getDao().saveSubscriptionEventDetais(subscriptionEvent)).thenCallRealMethod();
        Mockito.when(dao.getCacheManager()).thenReturn(cacheManager);
        Mockito.when(cacheManager.getCache(AppConstants.SUBSCRIPTION_EVENT_CACHE)).thenReturn(Mockito.any());
        Mockito.when(subcriptionService.saveSubscriptionEventDetais(subscriptionEvent)).thenCallRealMethod();
        EventResponse actualResponse = subcriptionService.saveSubscriptionEventDetais(subscriptionEvent);
        Assert.assertEquals(ErrorCodes.USER_ALREADY_EXISTS.getMessage(), actualResponse.getMessage());
        Assert.assertEquals(TEST_ACC_ID, actualResponse.getAccountIdentifier());
        Assert.assertEquals(false, actualResponse.isSuccess());
    }

    @Test
    public void testSaveSubscriptionEventDetaisForValidUser() {
        SubscriptionEvent subscriptionEvent = new SubscriptionEvent();
        Account account = new Account();
        account.setAccountIdentifier(TEST_ACC_ID);
        subscriptionEvent.setAccount(account);
        Creator creator = new Creator();
        creator.setFirstName(TEST_NAME);
        subscriptionEvent.setCreator(creator);
        Mockito.when(subcriptionService.fetchSubscriptionEventDetails(TEST_URL)).thenReturn(subscriptionEvent);
        Mockito.when(subcriptionService.getDao()).thenReturn(dao);
        Mockito.when(subcriptionService.getDao().fetchValueWrapperForEvent(AppConstants.SUBSCRIPTION_EVENT_CACHE, TEST_ACC_ID)).thenReturn(null);
        Mockito.when(subcriptionService.getDao().saveSubscriptionEventDetais(subscriptionEvent)).thenCallRealMethod();
        Mockito.when(dao.getCacheManager()).thenReturn(cacheManager);
        EventResponse expected = createExpectedResult();
        Mockito.when(subcriptionService.saveSubscriptionEventDetais(subscriptionEvent)).thenReturn(expected);
        EventResponse actualResponse = subcriptionService.saveSubscriptionEventDetais(subscriptionEvent);
        Assert.assertEquals(expected, actualResponse);
    }
    
    public void testUpdateSubscriptionEventDetaisForValidEvent(){
        //TODO
    }
    
    public void testUpdateSubscriptionEventDetaisForInvalidValidEvent(){
        //TODO
    }
    
    public void testCancelSubscriptionEventDetaisForValidEvent(){
        //TODO
    }
    
    public void testCancelSubscriptionEventDetaisForInvalidValidEvent(){
        //TODO
    }

    private EventResponse createExpectedResult() {
        EventResponse expected = new EventResponse();
        expected.setAccountIdentifier(TEST_ACC_ID);
        expected.setErrorCode(HttpStatus.OK.toString());
        expected.setSuccess(true);
        expected.setMessage(AppConstants.SUBS_ORDER_SUCCESS);
        return expected;
    }
}