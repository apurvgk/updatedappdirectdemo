<!DOCTYPE html>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE = edge">
<meta name="viewport" content="width = device-width, initial-scale = 1">
<title>MoneyManager</title>
<!-- Bootstrap -->
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script
		src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

	<div class="container text-center">
		<h2>General Information</h2>
		<spring:url value="/saveUserInfo" var="saveGeneralInfoURL" />
		<form:form class="form-horizontal" method="post"
			modelAttribute="user" action="${saveGeneralInfoURL}">
			<div class="form-group">
				<form:input path="userId" type="text" class="form-control"
					id="userId" placeholder="user ID" />
				<form:errors path="userId" class="control-label" />
			</div>
			<div class="form-group">
				<form:input path="firstName" type="text" class="form-control" id="firstNameId"
					placeholder="First Name" />
				<form:errors path="firstName" class="control-label" />
			</div>
			<div class="form-group">
				<form:input path="lastName" type="text" class="form-control" id="lastNameId"
					placeholder="Last Name" />
				<form:errors path="lastName" class="control-label" />
			</div>			
			<div class="form-group">
				<form:input path="phoneNumber" type="text" class="form-control"
					id="phoneId" placeholder="Mobile number" />
				<form:errors path="phoneNumber" class="control-label" />
			</div>
			<div class="form-group">
				<form:input path="emailId" type="text" class="form-control"
					id="emailId" placeholder="Email Id" />
				<form:errors path="emailId" class="control-label" />
			</div>
			<button type="submit" class="btn btn-success">Submit</button>
		</form:form>
	</div>
</body>
</html>