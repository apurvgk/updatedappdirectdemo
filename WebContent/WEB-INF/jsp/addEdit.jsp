<!DOCTYPE html>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE = edge">
<meta name="viewport" content="width = device-width, initial-scale = 1">
<title>AddOrEditTransaction</title>
<!-- Bootstrap -->
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script
		src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

	<div class="container text-center">
		<h2>Add or Edit Transaction</h2>
		<spring:url value="/saveTransactions" var="saveTransactionsUrl" />
		<form:form class="form-horizontal" method="post"
			modelAttribute="transaction" action="${saveTransactionsUrl}">
			<div class="form-group">
				<form:input path="title" type="text" class="form-control"
					id="titleId" placeholder="Enter title for transaction" />
				<form:errors path="title" class="control-label" />
			</div>
			<div class="form-group">
				<form:input path="initiator" type="text" class="form-control"
					id="initiatorId" placeholder="Enter initiator name" />
				<form:errors path="initiator" class="control-label" />
			</div>
			<div class="form-group">
				<form:input path="recipient" type="text" class="form-control"
					id="recipientId" placeholder="Enter recipient name" />
				<form:errors path="recipient" class="control-label" />
			</div>
			<div class="form-group">
				<form:input path="amount" type="text" class="form-control"
					id="amountId" placeholder="Enter amount" />
				<form:errors path="amount" class="control-label" />
			</div>
			<div class="form-group">
				<form:input path="creationDate" type="text" class="form-control"
					id="creationDateId" placeholder="dd-mm-yyyy" />
				<form:errors path="creationDate" class="control-label" />
			</div>
			<div class="form-group">
				<form:input path="description" type="text" class="form-control"
					id="descriptionId" placeholder="Enter description" />
				<form:errors path="description" class="control-label" />
			</div>
			<div class="form-group">
				<form:input path="status" type="text" class="form-control"
					id="statusId" placeholder="COMPLETE/PENDING" />
				<form:errors path="status" class="control-label" />
			</div>
			<div class="form-group">
				<form:input path="type" type="text" class="form-control" id="typeId"
					placeholder="GIVE/TAKE" />
				<form:errors path="type" class="control-label" />
			</div>
			<form:hidden path="userId" value='${userId}'></form:hidden>
			<button type="submit" class="btn btn-success">SAVE</button>
		</form:form>
	</div>
</body>
</html>