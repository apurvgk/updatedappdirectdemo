<!DOCTYPE html>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%><html
	lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE = edge">
<meta name="viewport" content="width = device-width, initial-scale = 1">
<title>MoneyManager</title>
<!-- Bootstrap -->
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body>
	<div id="result">
		<H1>Transaction Details</H1>
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script
			src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script
			src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
		<spring:url value="/editTransaction" var="editTransaction" />
		<form:form class="form-horizontal" method="post"
			modelAttribute="transaction" action="${editTransaction}">
			<div class="form-group">
				<form:input path="title" type="text" class="form-control"
					id="titleId" placeholder="Enter title for transaction" />
				<form:errors path="title" class="control-label" />
			</div>
			<div class="form-group">
				<form:input path="initiator" type="text" class="form-control"
					id="initiatorId" placeholder="Enter Initiator name" />
				<form:errors path="initiator" class="control-label" />
			</div>
			<div class="form-group">
				<form:input path="recipient" type="text" class="form-control"
					id="recipientId" placeholder="Enter recepient name" />
				<form:errors path="recipient" class="control-label" />
			</div>
			<div class="form-group">
				<form:input path="amount" type="text" class="form-control"
					id="amountId" placeholder="Enter amount" />
				<form:errors path="amount" class="control-label" />
			</div>
			<div class="form-group">
				<form:input path="creationDate" type="text" class="form-control"
					id="dateId" placeholder="dd-mm-yyyy" />
				<form:errors path="creationDate" class="control-label" />
			</div>
			<div class="form-group">
				<form:input path="description" type="text" class="form-control"
					id="descriptionId" placeholder="Enter description" />
				<form:errors path="description" class="control-label" />
			</div>
			<div class="form-group">
				<form:input path="type" type="text" class="form-control"
					id="transactionTypeId" placeholder="GIVE/TAKE" />
				<form:errors path="type" class="control-label" />
			</div>
			<form:hidden path="userId" value='${transaction.userId}'></form:hidden>
			<form:hidden path="transactionId"
				value='${transaction.transactionId}'></form:hidden>
			<button type="submit" class="btn btn-success">EDIT</button>
			<div class="btn-group">
				<button type="button" class="btn btn-warning"
					onclick="backToViewDetails('${transaction.userId}')">BACK</button>
			</div>
		</form:form>
	</div>
</body>

<script type="text/javascript">
	function backToViewDetails(userId) {
		$.ajax({
			type : "GET",
			url : "/moneyManager/viewTransactionsFromDetails?userId=" + userId,
			success : function(result) {
				$('#result').html(result);
			},
			error : function(result) {
			}
		});
	}
</script>
</html>